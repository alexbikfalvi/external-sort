package com.bikfalvi.sorter;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CoderResult;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;

/**
 * Implements the external sorting algorithm with a single character buffer.
 * The size of the buffer is set to {@code BUFFER_HEAP_RATIO} of the maximum
 * heap size for the current JVM, by default 60 percent. The algorithm works
 * with input lines up to the length of the buffer size, and otherwise throws
 * an exception. The maximum allowed buffer size is 2^31-1.
 *
 * The algorithm uses two passes. The sorting pass uses heap sort to ensure
 * O(1) additional memory used for sorting. The merge pass uses simple K-way
 * merging. The algorithm ignores the empty lines, which are not sorted or
 * written to the output file.
 *
 * The text data is assumed to be encoded using the character encoding specified
 * by the {@code "sorter.encoding"} system property, and the default is UTF-8.
 *
 * Possible improvements include: add line length information to the
 * intermediate temporary files, such that we don't recompute the lines lengths
 * during the merging pass. The merging pass can be optimized using a min-heap
 * for when the sorted text is divided into a large number of partitions.
 *
 * @author Alex Bikfalvi
 */
public class ExternalSort {

    private static final Boolean DEBUG = true;

    private static final String CHAR_ENCODING;
    private static final CharsetEncoder CHARSET_ENCODER;
    private static final CharsetDecoder CHARSET_DECODER;

    private static final float BUFFER_HEAP_RATIO = 0.6f;
    private static final int IO_BUFFER_SIZE = 0x10000;
    private static final int AVERAGE_LINE_LENGTH = 169;
    private static final long INDEX_MASK = 0x000000007FFFFFFFL;

    private static final String TEMP_PATH;

    private final Path inputPath;
    private final Path outputPath;
    private final ByteBuffer inputBuffer;
    private final ByteBuffer outputBuffer;
    private final CharBuffer dataBuffer;
    private final long[] lineIndex;
    private final int[] lineOrder;

    private long totalCharCount;
    private long totalLineCount;
    private int partitions;

    static {
        CHAR_ENCODING = System.getProperty("sorter.encoding", "UTF-8");
        CHARSET_ENCODER = Charset.forName(CHAR_ENCODING).newEncoder();
        CHARSET_DECODER = Charset.forName(CHAR_ENCODING).newDecoder();
        TEMP_PATH = System.getProperty("java.io.tmpdir");
    }

    /**
     * Program {@code main} method.
     *
     * @param args Arguments must include the following: the input file path,
     *             the output file path.
     */
    public static void main(String[] args) {
        if (args.length != 2) {
            System.err.println("Arguments are: <input-file> <output-file>");
            return;
        }

        try {
            long startTime = System.currentTimeMillis();
            ExternalSort sorter = new ExternalSort(args[0], args[1], 0, 0, 0);
            sorter.sort();
            long endTime = System.currentTimeMillis();
            if (DEBUG) {
                System.out.format("Sorting completed in %d ms",
                                  endTime - startTime);
            }
        } catch (Throwable e) {
            System.err.format("Sorting failed: %s\n", e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Performs an external sort on the text from the specified input path,
     * and writes the sorted lines to the specified output path.
     *
     * @param inputPath The input path from where the text lines are read.
     * @param outputPath The output path to where the text lines are written.
     * @param ioBufferSize The size of the buffer allocated for file I/O
     *                     operations. If negative or zero, the program will
     *                     select an I/O buffer size equal to
     *                     {@code IO_BUFFER_SIZE}.
     * @param dataBufferSize The size of the character buffer. If negative or
     *                       zero, the program will select a data buffer size
     *                       equal to the maximum heap size selected with the
     *                       -Xmx switch times the {@code BUFFER_HEAP_RATIO}
     *                       minus the size of the two buffers allocated for
     *                       file I/O.
     * @param lineIndexSize The size of the index where the order of the lines
     *                      are stored during the sorting algorithm. This
     *                      determines the number of lines that can be sorted
     *                      together. If negative or zero, the program will
     *                      select a lines index size equal to the data buffer
     *                      size divided to {@code AVERAGE_LINE_LENGTH}.
     * @throws IOException
     */
    public ExternalSort(String inputPath, String outputPath, int ioBufferSize,
                        int dataBufferSize, int lineIndexSize)
        throws IOException {

        this.inputPath = FileSystems.getDefault().getPath(inputPath);
        this.outputPath = FileSystems.getDefault().getPath(outputPath);

        inputBuffer = ByteBuffer.allocate(IO_BUFFER_SIZE);
        outputBuffer = ByteBuffer.allocate(IO_BUFFER_SIZE);

        long maxMem = Runtime.getRuntime().maxMemory();
        if (dataBufferSize <= 0) {
            dataBufferSize = (int) (Long.min(
                ((long) (maxMem * BUFFER_HEAP_RATIO) >>> 1),
                (maxMem - (IO_BUFFER_SIZE << 2))) & ~(1 << 31));
        }
        if (lineIndexSize <= 0) {
            lineIndexSize = dataBufferSize / AVERAGE_LINE_LENGTH;
        }
        if (DEBUG) {
            System.out.format("Maximum memory: %d\n", maxMem);
            System.out.format("Data buffer size: %d\n", dataBufferSize);
            System.out.format("Line index size: %d\n", lineIndexSize);
        }

        lineIndex = new long[lineIndexSize];
        lineOrder = new int[lineIndexSize];
        dataBuffer = CharBuffer.allocate(dataBufferSize);
    }

    /**
     * Sorts the lines from the input file and writes the result to the output
     * file.
     * @throws IOException
     */
    public void sort() throws Exception {
        CoderResult result;
        FileChannel inputChannel = FileChannel.open(inputPath);
        try {
            totalCharCount = 0L;
            totalLineCount = 0L;
            partitions = 0;
            int bytesRead;
            do {
                do {
                    bytesRead = inputChannel.read(inputBuffer);
                    inputBuffer.flip();
                    result = CHARSET_DECODER.decode(inputBuffer, dataBuffer, false);
                    inputBuffer.compact();
                } while (result.isUnderflow() && bytesRead != -1);
                sortBuffer();
            } while (result.isOverflow());
            if (DEBUG) {
                System.out.format("Sorted %d lines (%d characters)\n",
                                  totalLineCount, totalCharCount);
            }
        } finally {
            inputChannel.close();
        }

        FileChannel outputChannel = FileChannel.open(outputPath,
                                                     StandardOpenOption.CREATE,
                                                     StandardOpenOption.WRITE);
        try {
            if (DEBUG) {
                System.out.format("Performing %d-way merge of sorted "
                                  + "partitions\n", partitions);
            }
            int bufferSize = dataBuffer.capacity() / partitions;
            int position = 0;
            FileChannel[] channels = new FileChannel[partitions];
            CharBuffer[] buffers = new CharBuffer[partitions];
            for (int partition = 0; partition < partitions; partition++) {
                Path path = FileSystems.getDefault()
                    .getPath(TEMP_PATH, "extsort-" + partition + ".txt");
                channels[partition] = FileChannel.open(path);

                dataBuffer.limit(position + bufferSize);
                dataBuffer.position(position);
                buffers[partition] = dataBuffer.slice();
                buffers[partition].clear();
                lineIndex[partition] = ((long) position) << 32;
                position += bufferSize;
            }

            try {
                merge(outputChannel, channels, buffers);
            } finally {
                for (int partition = 0; partition < partitions; partition++) {
                    channels[partition].close();
                    Files.delete(FileSystems.getDefault()
                                     .getPath(TEMP_PATH, "extsort-" + partition +
                                                         ".txt"));
                }
            }
        } finally {
            outputChannel.close();
        }
    }

    /**
     * Performs a K-way merge between the specified input channels, and outputs
     * the resulting merged line set to the output channel.
     * @param outputChannel The output channel.
     * @param channels The input channels.
     * @param buffers The input buffers. These buffers leverage the globally
     *                allocated data buffer, such that no additional memory
     *                allocation is necessary.
     * @throws Exception
     */
    private void merge(FileChannel outputChannel, FileChannel[] channels,
                       CharBuffer[] buffers) throws Exception {
        int bytesRead;
        int lineCount = 0;
        CoderResult result;
        CoderResult[] results = new CoderResult[partitions];
        boolean readable;
        for (int partition = 0; partition < partitions; partition++) {
            results[partition] = CoderResult.OVERFLOW;
        }
        do {
            // Read each partition from the corresponding channel.
            for (int partition = 0; partition < partitions; partition++) {
                if (!results[partition].isOverflow()) {
                    buffers[partition].flip();
                    continue;
                }
                inputBuffer.clear();
                do {
                    bytesRead = channels[partition].read(inputBuffer);
                    inputBuffer.flip();
                    results[partition] =
                        CHARSET_DECODER.decode(inputBuffer, buffers[partition],
                                               false);
                    inputBuffer.compact();
                } while (results[partition].isUnderflow() && bytesRead != -1);
                if (inputBuffer.position() > 0) {
                    channels[partition].position(
                        channels[partition].position() - inputBuffer.position());
                }
                buffers[partition].flip();
            }

            // Mark the position of the end-of-line. This step can be avoided
            // if we add the line information to the temporary file, which was
            // already computed during the sorting pass.
            Arrays.fill(lineIndex, 0, partitions, 0L);
            for (int partition = 0; partition < partitions; partition++) {
                findNextLine(partition, buffers);

                if ((lineIndex[partition] >>> 32) ==
                    (lineIndex[partition] & INDEX_MASK)) {
                    // Sanity-check: the line cannot exceed the buffer size.
                    throw new Exception("Line too long");
                }
            }

            // Check if no more data is available from the merge partitions.
            readable = canRead(results);

            while (hasData()) {
                // Find the smallest line from each partition.
                int minimum = -1;
                for (int partition = 0; partition < partitions; partition++) {
                    // Skip the buffers with no data.
                    if ((lineIndex[partition] >> 32) >=
                        (lineIndex[partition] & INDEX_MASK)) {
                        continue;
                    }
                    // If this is the first buffer with data, choose this buffer
                    // else compare with the minimum.
                    if (minimum < 0) {
                        minimum = partition;
                    } else if (compareLines(minimum, partition,
                                            buffers[minimum].arrayOffset(),
                                            buffers[partition].arrayOffset()) > 0) {
                        minimum = partition;
                    }
                }

                // Output the minimum line.
                int begin = (int) (lineIndex[minimum] >>> 32);
                int length = (int) ((lineIndex[minimum] & INDEX_MASK) - begin);
                begin += buffers[minimum].arrayOffset();
                CharBuffer lineBuffer =
                    CharBuffer.wrap(dataBuffer.array(), begin, length);
                do {
                    outputBuffer.clear();
                    result =
                        CHARSET_ENCODER.encode(lineBuffer, outputBuffer, false);
                    outputBuffer.flip();
                    outputChannel.write(outputBuffer);
                } while (result == CoderResult.OVERFLOW);
                lineCount++;

                lineIndex[minimum] = lineIndex[minimum] << 32;

                int pos = findNextLine(minimum, buffers);

                // If any of the buffer reaches its limit, and reading has not
                // yet completed, compact all buffers and fetch more data from
                // the partition files.
                if (pos == buffers[minimum].limit() && readable) {
                    for (int partition = 0; partition < partitions; partition++) {
                        buffers[partition].position((int)(lineIndex[partition] &
                                                          INDEX_MASK));
                        buffers[partition].compact();
                    }
                    break;
                }
            }
        } while (readable);

        if (DEBUG) {
            System.out.format("Merged %d lines\n", lineCount);
        }
    }

    /**
     * @return {@code true} if all merge buffers are in underflow state, meaning
     *         that no more data is available to read from the partition files,
     *         {@code false} if data is available from at least one partition.
     */
    private boolean canRead(CoderResult[] results) {
        for (CoderResult result : results) {
            if (result.isOverflow()) {
                return true;
            }
        }
        return false;
    }

    private boolean hasData() {
        for (int partition = 0; partition < partitions; partition++) {
            if ((lineIndex[partition] >> 32) <
                (lineIndex[partition] & INDEX_MASK)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Finds the end-of-line index for line of text staring at {@code lineBegin}
     * position for the specified partition. The method updates the
     * {@code lineIndex} with the line indices.
     *
     * @param partition The partition index.
     * @param buffers The set of partition buffers.
     * @return The next index in the buffer to be checked.
     */
    private int findNextLine(int partition, CharBuffer[] buffers) {
        int pos = (int)(lineIndex[partition] >>> 32);
        while (pos < buffers[partition].limit()) {
            if (isEol(buffers[partition].get(pos++))) {
                lineIndex[partition] |= pos;
                return pos;
            }
        }
        lineIndex[partition] |= lineIndex[partition] >>> 32;
        return pos;
    }

    /**
     * Sorts the lines from the current data buffer. The method iterates over
     * the buffer to identify the character lines, and mark their positions
     * in the line index.
     *
     * It is possible that the capacity of the lines index (computed based on
     * the average line length {@code AVERAGE_LINE_LENGTH}) does not accommodate
     * all the lines from the buffer. In this case, the method performs several
     * sorting rounds until all the complete lines from the buffer are processed.
     *
     * After sorting, and remaining characters that are not part of the complete
     * line are copied at the beginning of the buffer and the buffer position
     * set after these characters.
     */
    private void sortBuffer() throws Exception {
        assert dataBuffer.limit() == dataBuffer.position();
        dataBuffer.flip();

        int charCount = dataBuffer.limit();
        boolean hasRemaining = dataBuffer.hasRemaining();
        char previousChar = '\0';
        char currentChar;
        // Iterate over the data buffer.
        while (hasRemaining) {
            // Reset the line index, and set the line counter to zero.
            Arrays.fill(lineIndex, 0L);
            lineIndex[0] = ((long) dataBuffer.position()) << 32;
            int lineCount = 0;
            while (dataBuffer.hasRemaining() && lineCount < lineIndex.length) {
                currentChar = dataBuffer.get();
                if (isEol(currentChar)) {
                    // If the current character if end-of-line, store the end
                    // line position in the line index.
                    if ((lineIndex[lineCount] & INDEX_MASK) == 0L) {
                        lineIndex[lineCount] |= dataBuffer.position();
                    }
                    lineCount++;
                    dataBuffer.mark();
                } else if (isEol(previousChar)) {
                    // If the current character is not end-of-line, but the
                    // previous was, store the begin line position in the line
                    // index.
                    lineIndex[lineCount] = ((long) dataBuffer.position() - 1) << 32;
                }
                previousChar = currentChar;
            }

            // Sort the current lines and write the result to a partitions
            // file.
            if (lineCount > 0) {
                sortLines(lineCount);
                writePartition(lineCount);
            }

            hasRemaining = dataBuffer.hasRemaining();
            // Reset the buffer after the previous end-of-line position to
            // continue next iteration from a new line. This is done here only
            // for logging below, otherwise it can be moved outside the loop.
            dataBuffer.reset();
            if (dataBuffer.position() == 0) {
                // Sanity-check: the line length cannot exceed the buffer size.
                throw new Exception("Line too long");
            }
            if (DEBUG) {
                System.out.format("Sorted %d lines (%d out of %d characters, "
                                  + "%d remaining)\n", lineCount,
                                  dataBuffer.position(), charCount,
                                  dataBuffer.remaining());
            }
            totalLineCount += lineCount;
        }
        dataBuffer.compact();
        totalCharCount += charCount - dataBuffer.position();
    }

    /**
     * @return True if the character is end-of-line, false otherwise.
     */
    private boolean isEol(char c) {
        return c == '\n' || c == '\r';
    }

    /**
     * Sorts the lines from the buffer using the current line index. The method
     * implements the heap sort algorithm, because it performs the sorting in
     * place and requires O(1) additional memory, and therefore it does affect
     * the maximum size of the data buffer.
     *
     * @param count The number of text lines to sort.
     */
    private void sortLines(int count) {
        for (int index = 0; index < count; index++) {
            lineOrder[index] = index;
        }

        for (int root = (count >> 1) - 1; root >= 0; root--) {
            sortHeap(root, count - 1);
        }

        sortSwap(0, count - 1);

        for (int index = count - 2; index > 0; index--) {
            sortHeap(0, index);
            sortSwap(0, index);
        }
    }

    /**
     * Lines sort auxiliary method. It swaps the order of two text lines.
     * @param left The index of the left line.
     * @param right The index of the right line.
     */
    private void sortSwap(int left, int right) {
        int temp = lineOrder[left];
        lineOrder[left] = lineOrder[right];
        lineOrder[right] = temp;
    }

    /**
     * Lines sort auxiliary method. It ensures the heap property on the current
     * line order.
     * @param root The heap root.
     * @param last The index of the last line.
     */
    private void sortHeap(int root, int last) {
        boolean done = false;
        int orphan = lineOrder[root];
        int left = (root << 1) + 1;

        while (!done && (left <= last)) {
            int larger = left;
            int right = left + 1;
            if (right <= last &&
                compareLines(lineOrder[right], lineOrder[larger], 0, 0) > 0) {
                larger = right;
            }
            if (compareLines(orphan, lineOrder[larger], 0, 0) < 0) {
                lineOrder[root] = lineOrder[larger];
                root = larger;
                left = (root << 1) + 1;
            } else {
                done = true;
            }
        }

        lineOrder[root] = orphan;
    }

    /**
     * Compares two text lines from the data buffer, according to the line
     * order specified in {@code lineOrder} and the line positions from the
     * {@code lineIndex}.
     *
     * @param leftIndex The index of the left line.
     * @param rightIndex The index of the right line.
     * @param leftOffset An offset in the left data buffer.
     * @param rightOffset An offset in the right data buffer.
     * @return {@code 0} if the two lines are lexicographically equal, less than
     *         {@code 0} if the left line is lexicographically less than the
     *         right line, and greater than {@code 0} if the left line is
     *         lexicographically greater than the right line.
     */
    private int compareLines(int leftIndex, int rightIndex,
                             int leftOffset, int rightOffset) {
        int leftBegin = (int)(lineIndex[leftIndex] >>> 32);
        int leftLength = (int)((lineIndex[leftIndex] & INDEX_MASK) -
                                leftBegin);
        int rightBegin = (int)(lineIndex[rightIndex] >>> 32);
        int rightLength = (int)((lineIndex[rightIndex] & INDEX_MASK) -
                                 rightBegin);
        int limit = Math.min(leftLength, rightLength);
        char data[] = dataBuffer.array();

        leftBegin += leftOffset;
        rightBegin += rightOffset;
        int leftIdx = leftBegin, rightIdx = rightBegin;
        while (leftIdx < leftBegin + limit && rightIdx < rightBegin + limit) {
            char c1 = data[leftIdx];
            char c2 = data[rightIdx];
            if (c1 != c2) {
                return c1 - c2;
            }
            leftIdx++;
            rightIdx++;
        }
        return leftLength - rightLength;
    }

    /**
     * Writes the text lines from the current buffer according to the current
     * line order to a partitions file, and increases the partition index.
     *
     * @param count The number of lines to write.
     */
    private void writePartition(int count) throws IOException {
        Path path = FileSystems.getDefault()
                               .getPath(TEMP_PATH, "extsort-" + partitions + ".txt");
        partitions++;
        FileChannel channel = FileChannel.open(path,
                                               StandardOpenOption.CREATE,
                                               StandardOpenOption.WRITE);
        try {
            CoderResult result;
            for (int index = 0; index < count; index++) {
                int begin = (int) (lineIndex[lineOrder[index]] >>> 32);
                int length = (int) ((lineIndex[lineOrder[index]] & INDEX_MASK) -
                                    begin);
                CharBuffer lineBuffer =
                    CharBuffer.wrap(dataBuffer.array(), begin, length);
                do {
                    outputBuffer.clear();
                    result =
                        CHARSET_ENCODER.encode(lineBuffer, outputBuffer, false);
                    outputBuffer.flip();
                    channel.write(outputBuffer);
                } while (result == CoderResult.OVERFLOW);
            }
        } finally {
            channel.close();
        }
    }
}
